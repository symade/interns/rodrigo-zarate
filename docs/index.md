# Hello everyone!
Welcome to my weekly blog about my research intership in Unilasalle Amiens, my name is Rodrigo and I will be your host.

The main focus is the multimaterial 3D printing usin a variety of techniques and the development of new tools to push the boundaries of 3D printing the farest away we can, and in order to accomplish that objective I will be using 3D modeling of pieces, 3D printers (obviously), microcontrollers, probably PCB design and a bunch of other stuffs.

All the pieces/parts/tools that I design are going to be tested and implemented in the printer below (notice the four interchangeable heads in the top).

![Hemera](./images/home/hemera.jpg){ align=left }

This is a really ambitious project and a challenge to me because I don't much experiences working in this area, but we are here to fix that.

In the first 2 or 3 weeks (hopefully just 2) this will seem like a poppurri of topics since I will be learning how to use all the necesary tools wich include the writing of this blog because (you guest it!) I'm learning also how to use this interface.

However, I'm confident in the results of this thanks to the support of my guide teacher (Jules Topart) and collegues.

You should pay him a visit! ({==Alert, this page is in french!==})

[Jules's Phd page ](https://julestoparr.gitlab.io/jules-thesis/){ .md-button .md-button--primary }

If you are one of the first reading this, congratulations, you will see a lot of modifications!